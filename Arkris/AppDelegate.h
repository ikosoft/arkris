//
//  AppDelegate.h
//  Arkris
//
//  Created by Brian Williams on 17/02/2013.
//  Copyright (c) 2013 Brian Williams. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
